// ***********************************************************************
// Assembly         : OMCS.YouTube.Contracts
// Author           : cgarcia
// Created          : 10-23-2017
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="YouTubeSourceType.cs" company="TVEyes Inc.">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************

namespace OMCS.YouTube.Contracts
{
    /// <summary>
    /// Enum YouTubeSourceType
    /// </summary>
    public enum YouTubeSourceType
    {
        /// <summary>
        /// The source of a <see cref="YouTubeAsset" /> has not been defined
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The source of a <see cref="YouTubeAsset" /> is a Channel
        /// </summary>
        YouTubeChannelSource,

        /// <summary>
        /// The source of a <see cref="YouTubeAsset" /> is a User
        /// </summary>
        YouTubeUserSource
    }
}
