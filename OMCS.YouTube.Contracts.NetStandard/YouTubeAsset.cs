﻿// ***********************************************************************
// Assembly         : OMCS.YouTube.Contracts
// Author           : cgarcia
// Created          : 10-23-2017
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="YouTubeAsset.cs" company="TVEyes Inc.">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using OMCS.Contracts;
using OMCS.YouTube.Contracts.Internal;

namespace OMCS.YouTube.Contracts
{
    /// <summary>
    /// Class YouTubeAsset.
    /// </summary>
    /// <seealso cref="OMCS.Contracts.IAsset" />
    public sealed class YouTubeAsset : IAsset
    {
        #region ctors

        /// <summary>
        /// Initializes a new instance of the <see cref="YouTubeAsset" /> class.
        /// </summary>
        /// <param name="assetId">The asset identifier.</param>
        public YouTubeAsset(string assetId)
        {
            this.AssetId = assetId;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="YouTubeAsset" /> class from being created.
        /// </summary>
        private YouTubeAsset()
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets the additional authors.
        /// </summary>
        /// <value>The additional authors.</value>
        public IList<string> AdditionalAuthors { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the asset identifier.
        /// </summary>
        /// <value>The asset identifier.</value>

        public string AssetId { get; }

        /// <summary>
        /// Gets or sets the state of the asset.
        /// </summary>
        /// <value>The state of the asset.</value>

        [JsonConverter(typeof(NullSafeAssetStateConverter))]
        public YouTubeAssetState AssetState { get; set; }

        /// <summary>
        /// Gets or sets the audience history.
        /// </summary>
        /// <value>The audience count history.</value>

        public IList<KeyValuePair<DateTime, long>> AudienceHistory { get; set; } =
            new List<KeyValuePair<DateTime, long>>();

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        /// <value>The author.</value>

        public string Author { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>The category.</value>

        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the character set.
        /// </summary>
        /// <value>The character set.</value>

        public string CharacterSet { get; set; }

        /// <summary>
        /// Gets or sets the comment count history.
        /// </summary>
        /// <value>The comment count history.</value>

        public IList<KeyValuePair<DateTime, int>> CommentHistory { get; set; } =
            new List<KeyValuePair<DateTime, int>>();

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>

        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>

        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the discovered on.
        /// </summary>
        /// <value>The discovered on.</value>

        public DateTime DiscoveryDate { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        /// <value>The duration.</value>

        public string Duration { get; set; }

        /// <summary>
        /// Gets or sets the industry.
        /// </summary>
        /// <value>The industry.</value>

        public string Industry { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is subscription based.
        /// </summary>
        /// <value><c>true</c> if this instance is subscription based; otherwise, <c>false</c>.</value>

        public bool IsSubscriptionBased { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>

        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the date the <seealso cref="YouTubeAsset" /> was last visited on.
        /// </summary>
        /// <value>The last visited.</value>

        public DateTime LastVisitedOn { get; set; }

        /// <summary>
        /// Gets or sets the license.
        /// </summary>
        /// <value>The license.</value>

        public string License { get; set; }

        /// <summary>
        /// Gets or sets the market.
        /// </summary>
        /// <value>The market.</value>

        public string Market { get; set; }

        /// <summary>
        /// Gets or sets the type of the media.
        /// </summary>
        /// <value>The type of the media.</value>

        public string MediaType { get; set; }

        /// <summary>
        /// Gets or sets the negative feedback.
        /// </summary>
        /// <value>The negative feedback.</value>

        public int NegativeFeedback { get; set; }

        /// <summary>
        /// Gets or sets the negative feedback history.
        /// </summary>
        /// <value>The negative feedback history.</value>

        public IList<KeyValuePair<DateTime, int>> NegativeFeedbackHistory { get; set; } =
            new List<KeyValuePair<DateTime, int>>();

        /// <summary>
        /// Gets or sets a <seealso cref="Tuple{T1,T2}" /> that represents this
        /// <see cref="YouTubeAsset" />'s position within a group of <seealso cref="YouTubeAsset" />s.
        /// </summary>
        /// <value>A <seealso cref="Tuple{Int16,Int16}" /> that represents this <see cref="YouTubeAsset" />'s
        /// position within a group of <seealso cref="YouTubeAsset" />s.</value>

        public Tuple<short, short> NofN { get; set; }

        /// <summary>
        /// Gets or sets the number of comments.
        /// </summary>
        /// <value>The number of comments.</value>

        public int NumberOfComments { get; set; }

        /// <summary>
        /// Gets or sets the number of times the <seealso cref="YouTubeAsset" /> has been visited.
        /// </summary>
        /// <value>The number of times visited.</value>

        public int NumberOfTimesVisited { get; set; }

        /// <summary>
        /// Gets or sets the origin identifier.
        /// </summary>
        /// <value>The origin identifier.</value>

        public string OriginId { get; set; }

        /// <summary>
        /// Gets or sets the popularity.
        /// </summary>
        /// <value>The popularity.</value>

        public string Popularity { get; set; }

        /// <summary>
        /// Gets or sets the positive feedback.
        /// </summary>
        /// <value>The positive feedback.</value>

        public int PositiveFeedback { get; set; }

        /// <summary>
        /// Gets or sets the positive feedback history.
        /// </summary>
        /// <value>The positive feedback history.</value>

        public IList<KeyValuePair<DateTime, int>> PositiveFeedbackHistory { get; set; } =
            new List<KeyValuePair<DateTime, int>>();

        /// <summary>
        /// Gets or sets the processed on.
        /// </summary>
        /// <value>The processed on.</value>

        public DateTime ProcessedDate { get; set; }

        /// <summary>
        /// Gets or sets the processing errors.
        /// </summary>
        /// <value>The processing errors.</value>

        public IList<YouTubeProcessingError> ProcessingErrors { get; set; } = new List<YouTubeProcessingError>();

        /// <summary>
        /// Gets the processing history.
        /// </summary>
        /// <value>The processing history.</value>

        public IList<string> ProcessingHistory { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the processing status.
        /// </summary>
        /// <value>The processing status.</value>

        [JsonConverter(typeof(NullSafeEnumValueConverter<ProcessingStatus>))]
        public ProcessingStatus ProcessingStatus { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>The priority.</value>

        public byte Priority { get; set; }

        /// <summary>
        /// Gets or sets the published on.
        /// </summary>
        /// <value>The published on.</value>

        public DateTime PublishDate { get; set; }

        /// <summary>
        /// Gets or sets the publisher.
        /// </summary>
        /// <value>The publisher.</value>

        public string Publisher { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        /// <value>The rank.</value>

        public long Rank { get; set; }

        /// <summary>
        /// Gets or sets the region.
        /// </summary>
        /// <value>The region.</value>

        public string Region { get; set; }

        /// <summary>
        /// Gets or sets the date the <seealso cref="YouTubeAsset" /> is eligible to be revisited.
        /// </summary>
        /// <value>The next revisit date.</value>

        public DateTime RevisitOn { get; set; }

        /// <summary>
        /// Gets or sets the source origin identifier.
        /// </summary>
        /// <value>The source origin identifier.</value>

        public string SourceId { get; set; }

        /// <summary>
        /// Gets or sets the source title.
        /// </summary>
        /// <value>The source title.</value>

        public string SourceTitle { get; set; }

        /// <summary>
        /// Gets or sets the type of the source.
        /// </summary>
        /// <value>The type of the source.</value>

        [JsonConverter(typeof(NullSafeEnumValueConverter<YouTubeSourceType>))]
        public YouTubeSourceType SourceType { get; set; }

        /// <summary>
        /// Gets or sets the source origin URL.
        /// </summary>
        /// <value>The source origin URL.</value>

        public string SourceUrl { get; set; }

        /// <summary>
        /// Gets or sets the path the <see cref="YouTubeAsset" /> has been physically stored (i.e. downloaded).
        /// </summary>
        /// <value>The asset physical path.</value>

        public string StoragePath { get; set; }

        /// <summary>
        /// Gets or sets the sub title.
        /// </summary>
        /// <value>The sub title.</value>

        public string SubTitle { get; set; }

        /// <summary>
        /// Gets the tags.
        /// </summary>
        /// <value>The tags.</value>

        public IList<string> Tags { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>

        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the asset transcript lines.
        /// </summary>
        /// <value>The asset transcript lines.</value>

        public IList<AssetTranscriptLine> TranscriptLines { get; set; } = new List<AssetTranscriptLine>();

        /// <summary>
        /// Gets or sets the transcript origin.
        /// </summary>
        /// <value>The transcript origin.</value>

        [JsonConverter(typeof(NullSafeEnumValueConverter<TranscriptOrigin>))]
        public TranscriptOrigin TranscriptOrigin { get; set; }

        /// <summary>
        /// Gets or sets the upload date.
        /// </summary>
        /// <value>The upload date.</value>

        public DateTime UploadDate { get; set; }

        /// <summary>
        /// Gets or sets the origin URL.
        /// </summary>
        /// <value>The origin URL.</value>

        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>

        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the views.
        /// </summary>
        /// <value>The views.</value>

        public string Views { get; set; }
    }
}
