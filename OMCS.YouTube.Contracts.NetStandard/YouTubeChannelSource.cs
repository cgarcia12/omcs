﻿// ***********************************************************************
// Assembly         : OMCS.YouTube.Contracts
// Author           : cgarcia
// Created          : 10-23-2017
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="YouTubeChannelSource.cs" company="TVEyes Inc.">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using OMCS.Contracts;

namespace OMCS.YouTube.Contracts
{
    /// <summary>
    /// Class YouTubeChannelSource. This class cannot be inherited.
    /// </summary>
    /// <seealso cref="OMCS.Contracts.IAssetSource" />

    public sealed class YouTubeChannelSource : IAssetSource
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="YouTubeChannelSource" /> class.
        /// </summary>
        /// <param name="sourceId">The source identifier.</param>
        public YouTubeChannelSource(string sourceId)
        {
            this.SourceId = sourceId;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="YouTubeChannelSource" /> class from being created.
        /// </summary>
        private YouTubeChannelSource()
        {
        }

        /// <summary>
        /// Gets or sets the added on.
        /// </summary>
        /// <value>The added on.</value>

        public DateTime AddedOn { get; set; }

        /// <summary>
        /// Gets or sets the Audience.
        /// </summary>
        /// <value>The Audience.</value>
        /// <example>1022</example>

        public long Audience { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>

        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>

        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the discovery date on.
        /// </summary>
        /// <value>The date of discovery.</value>

        public DateTime DiscoveryDate { get; set; }

        /// <summary>
        /// Gets or sets the feed URL.
        /// </summary>
        /// <value>The feed URL.</value>

        public string FeedUrl { get; set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        /// <value>The image URL.</value>

        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is protected.
        /// </summary>
        /// <value><c>true</c> if this instance is protected; otherwise, <c>false</c>.</value>

        public bool IsProtected { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>

        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the last visited.
        /// </summary>
        /// <value>The last scan request.</value>

        public DateTime LastVisited { get; set; }

        /// <summary>
        /// Gets or sets the origin identifier of the <seealso cref="IAssetSource" />.
        /// </summary>
        /// <value>The origin identifier.</value>
        /// <example>"VanossGaming"</example>

        public string OriginId { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>The priority.</value>

        public byte Priority { get; set; }

        /// <summary>
        /// Gets or sets the processing history.
        /// </summary>
        /// <value>The processing history.</value>

        public IList<string> ProcessingHistory { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the identifiers that this <seealso cref="IAssetSource" /> is/are protected by.
        /// </summary>
        /// <value>The protected by.</value>

        public string[] ProtectedBy { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        /// <value>The rank.</value>

        public long Rank { get; set; }

        /// <summary>
        /// Gets or sets the social links.
        /// </summary>
        /// <value>The social links.</value>

        public string[] SocialLinks { get; set; }

        /// <summary>
        /// Gets the instance-defined source identifier as a <seealso cref="string" />.
        /// </summary>
        /// <value>The instance-defined source identifier as a <seealso cref="string" />.</value>

        public string SourceId { get; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>

        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the total views.
        /// </summary>
        /// <value>The total views.</value>

        public long TotalViews { get; set; }

        /// <summary>
        /// Gets or sets the origin URL of the <seealso cref="IAssetSource" />.
        /// </summary>
        /// <value>The origin source URL.</value>
        /// <example>"https://www.youtube.com/user/VanossGaming"</example>

        public string Url { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="YouTubeChannelSource" /> is verified.
        /// </summary>
        /// <value><c>true</c> if verified; otherwise, <c>false</c>.</value>

        public bool Verified { get; set; }
    }
}
