﻿// ***********************************************************************
// Assembly         : OMCS.YouTube.Contracts
// Author           : cgarcia
// Created          : 04-12-2018
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="NullSafeEnumValueConverter.cs" company="TVEyes Inc.">
//     Copyright (c) TVEyes Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OMCS.YouTube.Contracts.Internal
{
    /// <summary>
    /// Class NullSafeEnumValueConverter. This class cannot be inherited.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="JsonConverter" />
    internal sealed class NullSafeEnumValueConverter<T> : JsonConverter<T>
    {
        /// <summary>
        /// Reads and converts the JSON to type <typeparamref name="T" />.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="typeToConvert">The type to convert.</param>
        /// <param name="options">An object that specifies serialization options to use.</param>
        /// <returns>The converted value.</returns>
        public override T Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options) =>
            (T)Enum.Parse(typeToConvert, reader.GetString());


        /// <summary>
        /// Writes the specified writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="assetState">State of the asset.</param>
        /// <param name="options">The options.</param>
        public override void Write(
            Utf8JsonWriter writer,
            T assetState,
            JsonSerializerOptions options) =>
            writer.WriteStringValue(assetState.ToString());
    }
}
