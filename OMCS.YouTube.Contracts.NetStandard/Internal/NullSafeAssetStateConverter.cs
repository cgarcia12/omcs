﻿// ***********************************************************************
// Assembly         : OMCS.YouTube.Contracts
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="NullSafeAssetStateConverter.cs" company="OMCS.YouTube.Contracts">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace OMCS.YouTube.Contracts.Internal
{
    /// <summary>
    /// Class NullSafeAssetStateConverter. This class cannot be inherited.
    /// </summary>
    /// <seealso cref="JsonConverter" />
    internal sealed class NullSafeAssetStateConverter : JsonConverter<YouTubeAssetState>
    {
        /// <summary>
        /// Reads and converts the JSON to type <typeparamref>
        ///     <name>T</name>
        /// </typeparamref>
        /// .
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <param name="typeToConvert">The type to convert.</param>
        /// <param name="options">An object that specifies serialization options to use.</param>
        /// <returns>The converted value.</returns>
        public override YouTubeAssetState Read(
            ref Utf8JsonReader reader,
            Type typeToConvert,
            JsonSerializerOptions options)
        {
            return (YouTubeAssetState)Enum.Parse(typeToConvert, reader.GetString());
        }

        /// <summary>
        /// Writes the specified writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="assetState">State of the asset.</param>
        /// <param name="options">The options.</param>
        public override void Write(
            Utf8JsonWriter writer,
            YouTubeAssetState assetState,
            JsonSerializerOptions options)
        {
            writer.WriteStringValue(assetState.ToString());
        }
    }
}
