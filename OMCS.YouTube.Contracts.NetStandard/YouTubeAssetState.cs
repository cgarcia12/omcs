﻿// ***********************************************************************
// Assembly         : OMCS.YouTube.Contracts
// Author           : cgarcia
// Created          : 02-22-2018
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="YouTubeAssetState.cs" company="TVEyes Inc.">
//     Copyright (c) TVEyes Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Runtime.Serialization;

namespace OMCS.YouTube.Contracts
{
    /// <summary>
    /// Enum AssetState
    /// </summary>
    public enum YouTubeAssetState
    {
        /// <summary>
        /// The state of an Asset has not been specified.
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The Asset is online.
        /// </summary>
        [EnumMember]
        Online,

        /// <summary>
        /// The Asset is offline
        /// </summary>
        [EnumMember]
        Offline,

        /// <summary>
        /// The Asset has been relocated
        /// </summary>
        [EnumMember]
        Relocated,

        /// <summary>
        /// The Asset has been removed
        /// </summary>
        [EnumMember]
        Removed,

        /// <summary>
        /// The Asset is not accessible. (Typically, due to authentication requirement).
        /// </summary>
        [EnumMember]
        Inaccessible
    }
}
