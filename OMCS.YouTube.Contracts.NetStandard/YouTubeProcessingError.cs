﻿// ***********************************************************************
// Assembly         : OMCS.YouTube.Contracts
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="YouTubeProcessingError.cs" company="OMCS.YouTube.Contracts">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace OMCS.YouTube.Contracts
{
    /// <summary>
    /// Class YouTubeProcessingError. This class cannot be inherited.
    /// </summary>
    public sealed class YouTubeProcessingError
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="YouTubeProcessingError"/> class from being created.
        /// </summary>
        private YouTubeProcessingError()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="YouTubeProcessingError" /> class.
        /// </summary>
        /// <param name="cause">The cause.</param>
        public YouTubeProcessingError(Exception cause)
        {
            this.Cause = cause;
            this.Reason = $"{DateTime.Now:u} | {Environment.MachineName} | {typeof(YouTubeAsset).FullName} | " + cause;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="YouTubeProcessingError"/> class.
        /// </summary>
        /// <param name="reason">The reason.</param>
        public YouTubeProcessingError(string reason)
        {
            this.Cause = new Exception(reason);
            this.Reason = $"{DateTime.Now:u} | {Environment.MachineName} | {typeof(YouTubeAsset).FullName} | " + reason;
        }

        /// <summary>
        /// Gets or sets the cause.
        /// </summary>
        /// <value>The cause.</value>
        public Exception Cause { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>The reason.</value>

        public string Reason { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return null == this.Cause ? this.Reason : $"{this.Reason}; {this.Cause}";
        }
    }
}
