﻿// ***********************************************************************
// Assembly         : OMCS.Contracts
// Author           : cgarcia
// Created          : 08-21-2019
//
// Last Modified By : cgarcia
// Last Modified On : 08-21-2019
// ***********************************************************************
// <copyright file="ProcessingStatus.cs" company="TVEyes Inc.">
//     Copyright © TVEyes Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System.Runtime.Serialization;

namespace OMCS.Contracts
{
    /// <summary>
    /// Enum ProcessingStatus
    /// </summary>
    public enum ProcessingStatus
    {
        /// <summary>
        /// The undefined
        /// </summary>
        [EnumMember]
        Undefined = 0,

        /// <summary>
        /// The cannot index invalid asset state
        /// </summary>
        [EnumMember]
        CannotIndexInvalidAssetState,

        /// <summary>
        /// The cannot index errors found
        /// </summary>
        
        CannotIndexAssetErrorsExist,

        /// <summary>
        /// The enhancing
        /// </summary>
        [EnumMember]
        Enhancing,

        /// <summary>
        /// The enhancement failed
        /// </summary>
        [EnumMember]
        EnhancementFailed,

        /// <summary>
        /// The revisited and enhanced
        /// </summary>
        
        RevisitedAndEnhanced,

        /// <summary>
        /// The enhanced
        /// </summary>
        
        Enhanced,

        /// <summary>
        /// The sent for enhancement
        /// </summary>
        [EnumMember]
        SentForEnhancement,

        /// <summary>
        /// The sent for indexing
        /// </summary>
        [EnumMember]
        SentForIndexing,

        /// <summary>
        /// The indexing
        /// </summary>
        [EnumMember]
        Indexing,

        /// <summary>
        /// The indexing failed
        /// </summary>
        [EnumMember]
        IndexingFailed,

        /// <summary>
        /// The indexed
        /// </summary>
        [EnumMember]
        Indexed
    }
}
