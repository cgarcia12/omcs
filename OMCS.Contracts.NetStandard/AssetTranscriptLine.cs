﻿namespace OMCS.Contracts
{
	/// <summary>
	/// Class AssetTranscriptLine.
	/// </summary>
	public sealed class AssetTranscriptLine
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AssetTranscriptLine" /> class.
		/// </summary>
		public AssetTranscriptLine()
		{
		}

		/// <summary>
		/// Gets or sets the offset in milliseconds.
		/// </summary>
		/// <value>The offset in milliseconds.</value>
		
		public double OffsetMs { get; set; }

		/// <summary>
		/// Gets or sets the duration in milliseconds.
		/// </summary>
		/// <value>The duration in milliseconds.</value>
		
		public double DurationMs { get; set; }

		/// <summary>
		/// Gets or sets the total ticks.
		/// </summary>
		/// <value>The total ticks.</value>
		/// <remarks>
		/// Use this value to convert the <see cref = "AssetTranscriptLine" /> duration into a <see cref = "System.TimeSpan" />.
		/// </remarks>
		
		public long TotalTicks { get; set; }

		/// <summary>
		/// Gets or sets the content.
		/// </summary>
		/// <value>The content.</value>
		
		public string Content { get; set; }		
	}
}