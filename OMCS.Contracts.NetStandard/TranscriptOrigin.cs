﻿// ***********************************************************************
// Assembly         : OMCS.Contracts
// Author           : cgarcia
// Created          : 04-09-2018
//
// Last Modified By : cgarcia
// Last Modified On : 04-09-2018
// ***********************************************************************
// <copyright file="TranscriptOrigin.cs" company="TVEyes Inc.">
//     Copyright (c) TVEyes Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System.Runtime.Serialization;

namespace OMCS.Contracts
{
    /// <summary>
    /// Enum TranscriptOrigin
    /// </summary>

    public enum TranscriptOrigin
    {
        /// <summary>
        /// The undefined
        /// </summary>
        [EnumMember]
        Undefined,

        /// <summary>
        /// The none
        /// </summary>
        [EnumMember]
        None,

        /// <summary>
        /// The closed captions
        /// </summary>
        [EnumMember]
        ClosedCaptions,

        /// <summary>
        /// The automatic speech recongnition
        /// </summary>
        [EnumMember]
        AutoSpeechRecognition
    }
}
