// ***********************************************************************
// Assembly         : OMCS.Contracts
// Author           : cgarcia
// Created          : 10-23-2017
//
// Last Modified By : cgarcia
// Last Modified On : 07-02-2019
// ***********************************************************************
// <copyright file="IAssetSource.cs" company="TVEyes Inc.">
//     Copyright �  2017
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;

namespace OMCS.Contracts
{
    /// <summary>
    /// Interface IAssetSource
    /// </summary>
    public interface IAssetSource
    {
        /// <summary>
        /// Gets or sets the added on.
        /// </summary>
        /// <value>The added on.</value>
        DateTime AddedOn { get; set; }

        /// <summary>
        /// Gets or sets the size of the audience (Number of subscribers)
        /// </summary>
        /// <value>The number of subscribers</value>
        /// <example>1022</example>
        long Audience { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        string Country { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        string Description { get; set; }

        /// <summary>
        /// Gets or sets the discovery date.
        /// </summary>
        /// <value>The added on.</value>
        DateTime DiscoveryDate { get; set; }

        /// <summary>
        /// Gets or sets the image URL.
        /// </summary>
        /// <value>The image URL.</value>
        string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is protected.
        /// </summary>
        /// <value><c>true</c> if this instance is protected; otherwise, <c>false</c>.</value>
        bool IsProtected { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>
        string Language { get; set; }

        /// <summary>
        /// Gets or sets the discovery date.
        /// </summary>
        /// <value>The added on.</value>
        DateTime LastVisited { get; set; }

        /// <summary>
        /// Gets or sets the origin identifier.
        /// </summary>
        /// <value>The origin identifier.</value>
        string OriginId { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>The priority.</value>
        byte Priority { get; set; }

        /// <summary>
        /// Gets or Sets the processing history.
        /// </summary>
        /// <value>The processing history.</value>
        IList<string> ProcessingHistory { get; set; }

        /// <summary>
        /// Gets or sets the identifiers that this <seealso cref="IAssetSource" /> is/are protected by.
        /// </summary>
        /// <value>The protected by.</value>
        string[] ProtectedBy { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        /// <value>The rank.</value>
        long Rank { get; set; }

        /// <summary>
        /// Gets the source identifier.
        /// </summary>
        /// <value>The identifier.</value>
        string SourceId { get; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the source URL.
        /// </summary>
        /// <value>The source URL.</value>
        /// <example>"https://www.youtube.com/user/VanossGaming"</example>
        string Url { get; set; }
    }
}
