﻿// ***********************************************************************
// Assembly         : OMCS.Contracts
// Author           : cgarcia
// Created          : 10-23-2017
//
// Last Modified By : cgarcia
// Last Modified On : 07-29-2019
// ***********************************************************************
// <copyright file="IAsset.cs" company="TVEyes Inc.">
//     Copyright ©  2017
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace OMCS.Contracts
{
    /// <summary>
    /// The public contract exposed by all Assets within the OMCS system.
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedMemberInSuper.Global")]
    public interface IAsset
    {
        /// <summary>
        /// Gets or sets the additional authors.
        /// </summary>
        /// <value>The additional authors.</value>
        IList<string> AdditionalAuthors { get; set; }

        /// <summary>
        /// Gets the asset identifier.
        /// </summary>
        /// <value>The asset identifier.</value>
        string AssetId { get; }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        /// <value>The author.</value>
        string Author { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>The category.</value>
        string Category { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>The country.</value>
        string Country { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        string Description { get; set; }

        /// <summary>
        /// Gets or sets the discovered on.
        /// </summary>
        /// <value>The discovered on.</value>
        DateTime DiscoveryDate { get; set; }

        /// <summary>
        /// Gets or sets the duration.
        /// </summary>
        /// <value>The duration.</value>
        string Duration { get; set; }

        /// <summary>
        /// Gets or sets the industry.
        /// </summary>
        /// <value>The industry.</value>
        string Industry { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is subscription based.
        /// </summary>
        /// <value><c>true</c> if this instance is subscription based; otherwise, <c>false</c>.</value>
        bool IsSubscriptionBased { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>
        string Language { get; set; }

        /// <summary>
        /// Gets or sets the last visited.
        /// </summary>
        /// <value>The last visited.</value>
        DateTime LastVisitedOn { get; set; }

        /// <summary>
        /// Gets or sets the license.
        /// </summary>
        /// <value>The license.</value>
        string License { get; set; }

        /// <summary>
        /// Gets or sets the market.
        /// </summary>
        /// <value>The market.</value>
        string Market { get; set; }

        /// <summary>
        /// Gets or sets the type of the media.
        /// </summary>
        /// <value>The type of the media.</value>
        string MediaType { get; set; }

        /// <summary>
        /// Gets or sets the origin identifier.
        /// </summary>
        /// <value>The origin identifier.</value>
        string OriginId { get; set; }

        /// <summary>
        /// Gets or sets the popularity.
        /// </summary>
        /// <value>The popularity.</value>
        string Popularity { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>The priority.</value>
        byte Priority { get; set; }

        /// <summary>
        /// Gets or sets the processed on.
        /// </summary>
        /// <value>The processed on.</value>
        DateTime ProcessedDate { get; set; }

        /// <summary>
        /// Gets the processing history.
        /// </summary>
        /// <value>The processing history.</value>
        IList<string> ProcessingHistory { get; set; }

        /// <summary>
        /// Gets or sets the processing status.
        /// </summary>
        /// <value>The processing status.</value>
        ProcessingStatus ProcessingStatus { get; set; }

        /// <summary>
        /// Gets or sets the published on.
        /// </summary>
        /// <value>The published on.</value>
        DateTime PublishDate { get; set; }

        /// <summary>
        /// Gets or sets the publisher.
        /// </summary>
        /// <value>The publisher.</value>
        string Publisher { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        /// <value>The rank.</value>
        long Rank { get; set; }

        /// <summary>
        /// Gets or sets the region.
        /// </summary>
        /// <value>The region.</value>
        string Region { get; set; }

        /// <summary>
        /// Gets or sets the source origin identifier.
        /// </summary>
        /// <value>The source origin identifier.</value>
        string SourceId { get; set; }

        /// <summary>
        /// Gets or sets the source title.
        /// </summary>
        /// <value>The source title.</value>
        string SourceTitle { get; set; }

        /// <summary>
        /// Gets or sets the source URL.
        /// </summary>
        /// <value>The source URL.</value>
        string SourceUrl { get; set; }

        /// <summary>
        /// Gets or sets the sub title.
        /// </summary>
        /// <value>The sub title.</value>
        string SubTitle { get; set; }

        /// <summary>
        /// Gets the tags.
        /// </summary>
        /// <value>The tags.</value>
        IList<string> Tags { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the asset transcript lines.
        /// </summary>
        /// <value>The asset transcript lines.</value>
        IList<AssetTranscriptLine> TranscriptLines { get; set; }

        /// <summary>
        /// Gets or sets the transcript origin.
        /// </summary>
        /// <value>The transcript origin.</value>
        TranscriptOrigin TranscriptOrigin { get; set; }

        /// <summary>
        /// Gets or sets the upload date.
        /// </summary>
        /// <value>The upload date.</value>
        DateTime UploadDate { get; set; }

        /// <summary>
        /// Gets or sets the URL where the Asset is located.
        /// </summary>
        /// <value>The origin URL.</value>
        string Url { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        string Version { get; set; }

        /// <summary>
        /// Gets or sets the views.
        /// </summary>
        /// <value>The views.</value>
        string Views { get; set; }
    }
}
