﻿// ***********************************************************************
// Assembly         : OMCS.Contracts
// Author           : cgarcia
// Created          : 02-24-2020
//
// Last Modified By : cgarcia
// Last Modified On : 02-24-2020
// ***********************************************************************
// <copyright file="IConfigurationContextAware.cs" company="TVEyes Inc.">
//     Copyright © TVEyes Inc.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;

namespace OMCS.Contracts
{
    /// <summary>
    /// Identifies an object that requires a set of configuration properties carried with the execution code path.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    // ReSharper disable once UnusedTypeParameter
    public interface IRequiresConfigurationContext<T> where T : class
    {
        void AssertContextExists();
    }

    /// <summary>
    /// Class MissingConfigurationContextException. This class cannot be inherited.
    /// Implements the <see cref="System.ApplicationException" />
    /// </summary>
    /// <seealso cref="System.ApplicationException" />

    public sealed class MissingContextException<T> : ApplicationException
    {
        private MissingContextException()
        {
        }

        public MissingContextException(Type assertingType)
            : base()
        {
            this.AssertingType = assertingType;
        }

        /// <summary>
        /// Gets the <c>System.Type</c> of <c>class</c> asserting a configuration context is present.
        /// </summary>
        /// <value>The type of the asserting.</value>
        
        public Type AssertingType { get; }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The error message.</value>
        
        public string ErrorMessage=>$"The {nameof(T)} was not found for {this.AssertingType}";
    }
}
