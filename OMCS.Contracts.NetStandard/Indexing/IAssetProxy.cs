﻿// ***********************************************************************
// Assembly         : OMCS.Contracts
// Author           : cgarcia
// Created          : 11-20-2017
//
// Last Modified By : cgarcia
// Last Modified On : 11-21-2017
// ***********************************************************************
// <copyright file="IAssetProxy.cs" company="TVEyes Inc.">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;

namespace OMCS.Contracts.Indexing
{
    /// <summary>
    /// The public interface for proxy classes used for Asset indexing.
    /// </summary>
    public interface IAssetProxy
    {
        /// <summary>
        /// Gets or sets the asset identifier.
        /// </summary>
        /// <value>The asset identifier.</value>
        
        string AssetId { get; set; }

        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        /// <value>The audience.</value>
        /// <example>11,000 viewers</example>
        
        long Audience { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        
        string Description { get; set; }

        /// <summary>
        /// Gets or sets the discovery date.
        /// </summary>
        /// <value>The discovery date.</value>
        
        DateTime DiscoveryDate { get; set; }

        /// <summary>
        /// Gets or sets the duration in milliseconds.
        /// </summary>
        /// <value>The duration in milliseconds.</value>
        
        int DurationMs { get; set; }

        /// <summary>
        /// Gets or sets the index date.
        /// </summary>
        /// <value>The index date.</value>
        
        DateTime IndexDate { get; set; }

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>
        
        string Language { get; set; }

        /// <summary>
        /// Gets or sets the metadata.
        /// </summary>
        /// <value>The metadata.</value>
        /// <remarks>This is a concatenation of all relevant searchable metadata/properties on a per-Asset basis.</remarks>
        
        string Metadata { get; set; }

        /// <summary>
        /// Gets or sets the origin identifier.
        /// </summary>
        /// <value>The origin identifier.</value>
        
        string OriginId { get; set; }

        /// <summary>
        /// Gets or sets the processing status.
        /// </summary>
        /// <value>The processing status.</value>
        
        string ProcessingStatus { get; set; }

        /// <summary>
        /// Gets or sets the publish date.
        /// </summary>
        /// <value>The publish date.</value>
        
        DateTime PublishDate { get; set; }

        /// <summary>
        /// Gets or sets the rank.
        /// </summary>
        /// <value>The rank.</value>
        /// <example>#1 podcast or #3 on YouTube</example>
        
        long Rank { get; set; }

        /// <summary>
        /// Gets or sets the source identifier.
        /// </summary>
        /// <value>The source identifier.</value>
        
        string SourceId { get; set; }

        /// <summary>
        /// Gets or sets the source title.
        /// </summary>
        /// <value>The source title.</value>
        /// <example>ex. "Steve's YouTube Channel"</example>
        
        string SourceTitle { get; set; }

        /// <summary>
        /// Gets or sets the source-type of the asset.
        /// </summary>
        /// <value>The type of the asset.</value>
        
        string SourceType { get; set; }

        /// <summary>
        /// Gets or sets the source URL.
        /// </summary>
        /// <value>The source URL.</value>
        
        string SourceUrl { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        
        string Title { get; set; }

        /// <summary>
        /// Gets or sets the transcript.
        /// </summary>
        /// <value>The transcript.</value>
        /// <remarks>This property contains a concatenation of all <see cref="AssetTranscriptLine.Content" /> values.</remarks>
        
        string Transcript { get; set; }

        /// <summary>
        /// Gets or sets the transcript lines.
        /// </summary>
        /// <value>The transcript lines.</value>
        /// <remarks>The individual <see cref="AssetTranscriptLine" /> instances are preserved for more precise look-ups.</remarks>
        
        IList<AssetTranscriptLine> TranscriptLines { get; set; }

        /// <summary>
        /// Gets or sets the transcript origin.
        /// </summary>
        /// <value>The transcript origin.</value>
        
        string TranscriptOrigin { get; set; }

        /// <summary>
        /// Gets or sets the origin URL.
        /// </summary>
        /// <value>The origin URL.</value>
        
        string Url { get; set; }
    }
}
