﻿// ***********************************************************************
// Assembly         : OMCS.Contracts
// Author           : cgarcia
// Created          : 12-05-2017
//
// Last Modified By : cgarcia
// Last Modified On : 12-20-2017
// ***********************************************************************
// <copyright file="ISourceProxy.cs" company="TVEyes Inc.">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;

namespace OMCS.Contracts.Indexing
{
	/// <summary>
	/// Interface ISourceProxy
	/// </summary>
	public interface ISourceProxy
	{
		/// <summary>
		/// Gets or sets the identifier used by ElasticSearch to set the "_id" value.
		/// </summary>
		/// <value>The identifier.</value>
		string Id { get; set; }
		
		/// <summary>
		/// Gets or sets the audience.
		/// </summary>
		/// <value>The audience.</value>
		long Audience { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>The description.</value>
		string Description { get; set; }

		/// <summary>
		/// Gets or sets the discovery date.
		/// </summary>
		/// <value>The discovery date.</value>
		DateTime DiscoveryDate { get; set; }

		/// <summary>
		/// Gets or sets the index date.
		/// </summary>
		/// <value>The index date.</value>
		DateTime IndexDate { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is protected.
		/// </summary>
		/// <value><c>true</c> if this instance is protected; otherwise, <c>false</c>.</value>
		bool IsProtected { get; set; }

		/// <summary>
		/// Gets or sets the language.
		/// </summary>
		/// <value>The language.</value>
		string Language { get; set; }

		/// <summary>
		/// Gets or sets the metadata.
		/// </summary>
		/// <value>The metadata.</value>
		string Metadata { get; set; }

		/// <summary>
		/// Gets or sets the origin identifier.
		/// </summary>
		/// <value>The origin identifier.</value>
		string OriginId { get; set; }

		/// <summary>
		/// Gets or sets the identifiers that this <seealso cref="IAssetSource"/> is/are protected by.
		/// </summary>
		/// <value>The protected by.</value>
		string[] ProtectedBy { get; set; }

		/// <summary>
		/// Gets or sets the rank.
		/// </summary>
		/// <value>The rank.</value>
		long Rank { get; set; }

		/// <summary>
		/// Gets or sets the source identifier.
		/// </summary>
		/// <value>The source identifier.</value>
		string SourceId { get; set; }

		/// <summary>
		/// Gets or sets the type of the source.
		/// </summary>
		/// <value>The type of the source.</value>
		string SourceType { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>The title.</value>
		string Title { get; set; }
		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>The URL.</value>
		string Url { get; set; }
    }
}