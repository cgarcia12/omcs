﻿// ***********************************************************************
// Assembly         : OMCS.API.Source
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-27-2020
// ***********************************************************************
// <copyright file="ErrorHandlerMiddleware.cs" company="OMCS.API.Source">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OMCS.API.Source.Middleware
{
    /// <summary>
    /// Class ErrorDetails. This class cannot be inherited.
    /// </summary>
    public sealed class ErrorDetails
    {
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public int StatusCode { get; set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    /// <summary>
    /// Class HttpStatusCodeException. This class cannot be inherited.
    /// Implements the <see cref="System.Exception" />
    /// </summary>
    /// <seealso cref="System.Exception" />
    public sealed class HttpStatusCodeException : Exception
    {
        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        /// <value>The status code.</value>
        public HttpStatusCode StatusCode { get; set; }
        /// <summary>
        /// Gets or sets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        public string ContentType { get; set; } = @"text/plain";

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpStatusCodeException" /> class.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        public HttpStatusCodeException(HttpStatusCode statusCode)
        {
            this.StatusCode = statusCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpStatusCodeException" /> class.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <param name="message">The message.</param>
        public HttpStatusCodeException(HttpStatusCode statusCode, string message)
            : base(message)
        {
            this.StatusCode = statusCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpStatusCodeException" /> class.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <param name="inner">The inner.</param>
        public HttpStatusCodeException(HttpStatusCode statusCode, Exception inner)
            : this(statusCode, inner.ToString())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpStatusCodeException" /> class.
        /// </summary>
        /// <param name="statusCode">The status code.</param>
        /// <param name="errorObject">The error object.</param>
        public HttpStatusCodeException(HttpStatusCode statusCode, JObject errorObject)
            : this(statusCode, errorObject.ToString())
        {
            this.ContentType = @"application/json";
        }
    }

    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    /// <summary>
    /// Class ErrorHandlingMiddleware. This class cannot be inherited.
    /// </summary>
    public sealed class ErrorHandlingMiddleware
    {
        /// <summary>
        /// The next
        /// </summary>
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorHandlingMiddleware" /> class.
        /// </summary>
        /// <param name="next">The next.</param>
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this._next = next;
        }

        /// <summary>
        /// Invokes the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await this._next(context);
            }
            catch(HttpStatusCodeException ex)
            {
                await this.HandleExceptionAsync(context, ex);
            }
            catch(Exception exceptionObj)
            {
                await this.HandleExceptionAsync(context, exceptionObj);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, HttpStatusCodeException exception)
        {
            context.Response.ContentType = "application/json";
            if(exception is { })
            {
                var result = new ErrorDetails {Message = exception.Message, StatusCode = (int)exception.StatusCode}
                   .ToString();
                context.Response.StatusCode = (int)exception.StatusCode;
                return context.Response.WriteAsync(result);
            }

            var errorDetails = new ErrorDetails
                               {
                                   Message = "Runtime Error",
                                   StatusCode = (int)HttpStatusCode.BadRequest
                               }.ToString();

            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return context.Response.WriteAsync(errorDetails);
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var errorDetails = new ErrorDetails
                               {
                                   Message = exception.Message,
                                   StatusCode = (int)HttpStatusCode.InternalServerError
                               }
               .ToString();
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return context.Response.WriteAsync(errorDetails);
        }
    }
}
