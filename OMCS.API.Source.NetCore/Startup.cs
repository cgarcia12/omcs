// ***********************************************************************
// Assembly         : OMCS.API.Source
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="Startup.cs" company="OMCS.API.Source">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OMCS.API.Source.Controllers;

namespace OMCS.API.Source
{
    /// <summary>
    /// Class Startup. This class cannot be inherited.
    /// </summary>
    public sealed class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // #
            // # @see: https://joonasw.net/view/aspnet-core-di-deep-dive
            // #
            services.AddControllers();
            services.AddSwaggerGen(c=>c.SwaggerDoc("v1", new OpenApiInfo {Title = "OMCS Source API", Version = "v1"}));
            //var settings = this.GetConfigSettings();
            //services.AddTransient(s=>new YouTubeSourceController(settings));
            //services.AddSingleton(settings);
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        
        // ReSharper disable once UnusedMember.Global
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // #
            // # NOTE: The order that the configuration settings are applied affects the
            // #       steps in the processing Pipeline
            // #
            
            if(env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"));

            // # Marks the position in the middleware pipeline where a routing decision is made
            app.UseRouting();

            //app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            // # Marks the position in the middleware where the selected endpoint is executed
            app.UseEndpoints(endpoints=>endpoints.MapControllers());
        }

        /// <summary>
        /// Enum TargetSystem
        /// </summary>
        private enum TargetSystem
        {
            /// <summary>
            /// The undefined
            /// </summary>
            Undefined = 0,
            /// <summary>
            /// You tube
            /// </summary>
            YouTube,
            /// <summary>
            /// The podcasts
            /// </summary>
            Podcasts
        }

        /// <summary>
        /// Gets the configuration settings.
        /// </summary>
        /// <returns>ApiConfigurationSettings.</returns>
        private ApiConfigurationSettings GetConfigSettings()
        {
            /*
                "ApiSettings": {
                    "YouTube": {
                      "Elastic:ConnectionEnvironmentVariable": "YOUTUBE_ELASTIC_CONNECTION_STRING",
                      "Elastic:Nodes": "https://omcs-prod-es.pouflex.tveyes.com:9210",
                      "Elastic:AuthPassword": "Ears314",
                      "Elastic:AuthUser": "omcs-admin",
                      "Elastic:SourceLocation": "sources",
                      "Elastic:DefaultPageSize": 5,
                      "Elastic:ScrollTimeOut": "2m",
                      "RabbitMq:ExchangeName": "SourceExchange",
                      "RabbitMq:EnhancementRoutingKey": "omcs.enhancement.youtube.source",
                      "RabbitMq:BrokerUri": "amqp://omcs-app:e6a9ae6a-d0de-431e-85a2-fcd0d8d9f620@flexa.pouflex.tveyes.com:5670/%2Fomcs"
                    },
                    "Podcast": {
                      "Elastic:ConnectionEnvironmentVariable": "PODCAST_ELASTIC_CONNECTION_STRING",
                      "Elastic:DefaultConnectionValue": "PODCAST_ELASTIC_CONNECTION_STRING",
                      "Elastic:Nodes": "http://hqespool01.tveyes.com:9200;http://hqespool02.tveyes.com:9200;http://hqespool03.tveyes.com:9200",
                      "Elastic:AuthPassword": "Ears314",
                      "Elastic:AuthUser": "omcs-admin",
                      "Elastic:SourceLocation": "sources",
                      "Elastic:DefaultPageSize": 5,
                      "elastic:ScrollTimeOut": "2m",
                      "RabbitMq:ExchangeName": "SourceExchange",
                      "RabbitMq:EnhancementRoutingKey": "omcs.enhancement.youtube.source",
                      "RabbitMq:BrokerUri": "amqp://omcs-app:e6a9ae6a-d0de-431e-85a2-fcd0d8d9f620@flexa.pouflex.tveyes.com:5670/%2Fomcs"
                    }
                  }
             */
            ApiConfigurationSettings settings = new ApiConfigurationSettings();
            
            IConfiguration config = new ConfigurationBuilder()
                                   .AddJsonFile("appsettings.json")
                                   .Build();

            Uri[] dataNodes;
            var systemName = Environment.GetEnvironmentVariable("OMCS_API_SYSTEM_NAME",
                                                                EnvironmentVariableTarget.Machine);
            var targetSystem = (TargetSystem)Enum.Parse(typeof(TargetSystem), systemName, true);
            var youtubeSettings = config.GetSection("ApiSettings:YouTube");
            var podcastSettings = config.GetSection("ApiSettings:Podcast");
            switch (targetSystem)
            {
                case TargetSystem.Undefined:
                {
                    break;
                }
                case TargetSystem.Podcasts:
                {
                    string envKey = podcastSettings["Elastic:ConnectionEnvironmentVariable"];
                    var env = Environment.GetEnvironmentVariable(envKey);
                    dataNodes =
                        !string.IsNullOrEmpty(env)
                            ? env.Split(';')
                                 .Select(s=>new Uri(s)).ToArray()
                            : podcastSettings["Elastic:Nodes"].Split(';')
                                                              .Select(s=>new Uri(s)).ToArray();

                    settings.DataEndpoints = dataNodes;
                    settings.IndexOrAliasName = podcastSettings["Elastic:SourceLocation"];
                    settings.ElasticAuthUserPassword = podcastSettings["Elastic:AuthUserPassword"];
                    settings.ElasticAuthUserName = podcastSettings["Elastic:AuthUserName"];
                    settings.DefaultPageSize = byte.Parse(podcastSettings["Elastic:DefaultPageSize"]);
                    settings.RabbitMqBrokerUri = new Uri(podcastSettings["RabbitMq:BrokerUri"]);
                    settings.RabbitMqEnhancementRoutingKey = podcastSettings["RabbitMq:EnhancementRoutingKey"];
                    settings.RabbitMqExchangeName = podcastSettings["RabbitMq:ExchangeName"];
                    settings.ScrollTimeout = podcastSettings["Elastic:ScrollTimeOut"];
                    break;
                }
                case TargetSystem.YouTube:
                {
                    string envKey = youtubeSettings["Elastic:ConnectionEnvironmentVariable"];
                    var env = Environment.GetEnvironmentVariable(envKey);
                    dataNodes =
                        !string.IsNullOrEmpty(env)
                            ? env.Split(';')
                                 .Select(s => new Uri(s)).ToArray()
                            : youtubeSettings["Elastic:Nodes"].Split(';')
                                                              .Select(s => new Uri(s)).ToArray();

                    settings.DataEndpoints = dataNodes;
                    settings.IndexOrAliasName = youtubeSettings["Elastic:SourceLocation"];
                    settings.ElasticAuthUserPassword = youtubeSettings["Elastic:AuthUserPassword"];
                    settings.ElasticAuthUserName = youtubeSettings["Elastic:AuthUserName"];
                    settings.DefaultPageSize = byte.Parse(youtubeSettings["Elastic:DefaultPageSize"]);
                    settings.RabbitMqBrokerUri = new Uri(youtubeSettings["RabbitMq:BrokerUri"]);
                    settings.RabbitMqEnhancementRoutingKey = youtubeSettings["RabbitMq:EnhancementRoutingKey"];
                    settings.RabbitMqExchangeName = youtubeSettings["RabbitMq:ExchangeName"];
                    settings.ScrollTimeout = youtubeSettings["Elastic:ScrollTimeOut"];
                    break;
                    }
            }
            return settings;
        }
    }
}
