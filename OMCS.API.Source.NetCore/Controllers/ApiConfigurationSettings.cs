﻿// ***********************************************************************
// Assembly         : OMCS.API.Source
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-27-2020
// ***********************************************************************
// <copyright file="ApiConfigurationSettings.cs" company="OMCS.API.Source">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;

namespace OMCS.API.Source.Controllers
{
    /// <summary>
    /// Class ApiConfigurationSettings. This class cannot be inherited.
    /// </summary>
    public sealed class ApiConfigurationSettings
    {
        /// <summary>
        /// Gets or sets the elastic connection environment variable.
        /// </summary>
        /// <value>The elastic connection environment variable.</value>
        public string ElasticConnectionEnvironmentVariable { get; set; }
        /// <summary>
        /// Gets or sets the data endpoints.
        /// </summary>
        /// <value>The data endpoints.</value>
        public Uri[] DataEndpoints { get; set; }
        /// <summary>
        /// Gets or sets the name of the elastic authentication user.
        /// </summary>
        /// <value>The name of the elastic authentication user.</value>
        public string ElasticAuthUserName { get; set; }
        /// <summary>
        /// Gets or sets the elastic authentication user password.
        /// </summary>
        /// <value>The elastic authentication user password.</value>
        public string ElasticAuthUserPassword { get; set; }
        /// <summary>
        /// Gets or sets the name of the index or alias.
        /// </summary>
        /// <value>The name of the index or alias.</value>
        public string IndexOrAliasName { get; set; }
        /// <summary>
        /// Gets or sets the default size of the page.
        /// </summary>
        /// <value>The default size of the page.</value>
        public byte DefaultPageSize { get; set; }
        /// <summary>
        /// Gets or sets the scroll timeout.
        /// </summary>
        /// <value>The scroll timeout.</value>
        public string ScrollTimeout { get; set; }
        /// <summary>
        /// Gets or sets the name of the rabbit mq exchange.
        /// </summary>
        /// <value>The name of the rabbit mq exchange.</value>
        public string RabbitMqExchangeName { get; set; }
        /// <summary>
        /// Gets or sets the rabbit mq enhancement routing key.
        /// </summary>
        /// <value>The rabbit mq enhancement routing key.</value>
        public string RabbitMqEnhancementRoutingKey { get; set; }
        /// <summary>
        /// Gets or sets the rabbit mq broker URI.
        /// </summary>
        /// <value>The rabbit mq broker URI.</value>
        public Uri RabbitMqBrokerUri { get; set; }
    }
}
