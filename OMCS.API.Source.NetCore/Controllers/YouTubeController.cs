﻿// ***********************************************************************
// Assembly         : OMCS.API.Source
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-27-2020
// ***********************************************************************
// <copyright file="YouTubeController.cs" company="OMCS.API.Source">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OMCS.YouTube.Contracts;

namespace OMCS.API.Source.Controllers
{
    /// <summary>
    /// public sealed Class YouTubeController.
    /// Implements the <see cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    [Route("api/[controller]")]
    [ApiController]
    public class YouTubeController : ControllerBase
    {
        /// <summary>
        /// Gets the name of the channel source by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>YouTubeChannelSource.</returns>
        [HttpGet("source/channel/name/{name}")]
        [Produces("application/json")]
        public YouTubeChannelSource GetChannelSourceByName(string name)
        {
            return new YouTubeChannelSource("id") {Title = name};
        }

        /// <summary>
        /// Gets the channel source by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>YouTubeChannelSource.</returns>
        [HttpGet("source/channel/id/{id}")]
        [Produces("application/json")]
        public YouTubeChannelSource GetChannelSourceById(string id)
        {
            return new YouTubeChannelSource(id);
        }

        /// <summary>
        /// Gets the name of the user source by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>YouTubeUserSource.</returns>
        [HttpGet("source/user/name/{name}")]
        [Produces("application/json")]
        public YouTubeUserSource GetUserSourceByName(string name)
        {
            return new YouTubeUserSource("id") { Title = name };
        }

        /// <summary>
        /// Gets the user source by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>YouTubeUserSource.</returns>
        [HttpGet("source/user/id/{id}")]
        [Produces("application/json")]
        public YouTubeUserSource GetUserSourceById(string id)
        {
            return new YouTubeUserSource(id);
        }


        /// <summary>
        /// Gets the name of the source by.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>JsonResult.</returns>
        [HttpGet("source/name/{name}")]
        [Produces("application/json")]
        public JsonResult GetSourceByName(string name)
        {
            string url = this.HttpContext.Request.Path;
            if(url.IndexOf("/channel", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                var ytc = new YouTubeChannelSource(Guid.NewGuid().ToString())
                          {
                              Title = name
                          };
                return new JsonResult(JsonConvert.SerializeObject(ytc));
            }

            var yus = new YouTubeChannelSource(Guid.NewGuid().ToString())
                      {
                          Title = name
                      };
            return new JsonResult(JsonConvert.SerializeObject(yus));
        }

        /// <summary>
        /// Gets the source by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>JsonResult.</returns>
        [HttpGet("source/id/{id}")]
        [Produces("application/json")]
        public JsonResult GetSourceById(string id)
        {
            string url = this.HttpContext.Request.Path;
            if (url.IndexOf("/channel", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                var ytc = new YouTubeChannelSource(id);
                return new JsonResult(JsonConvert.SerializeObject(ytc));
            }

            var yus = new YouTubeChannelSource(id);
            return new JsonResult(JsonConvert.SerializeObject(yus));
        }

        /// <summary>
        /// Gets the source by URL.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns>JsonResult.</returns>
        [HttpGet("source/url/{url}")]
        [Produces("application/json")]
        public JsonResult GetSourceByUrl(string url)
        {
            if (url.IndexOf("/channel", StringComparison.InvariantCultureIgnoreCase) > -1)
            {
                var ytc = new YouTubeChannelSource(Guid.NewGuid().ToString());
                return new JsonResult(JsonConvert.SerializeObject(ytc));
            }

            var yus = new YouTubeChannelSource(Guid.NewGuid().ToString());
            return new JsonResult(JsonConvert.SerializeObject(yus));
        }
    }
}
