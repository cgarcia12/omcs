﻿// ***********************************************************************
// Assembly         : OMCS.Shared.
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-27-2020
// ***********************************************************************
// <copyright file="TypeAwareContractResolver.cs" company="OMCS.Shared.">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using Nest;
using Nest.JsonNetSerializer;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace OMCS.Shared.Elastic
{
    /// <summary>
    /// Class TypeAwareContractResolver.
    /// Implements the <see cref="Nest.JsonNetSerializer.ConnectionSettingsAwareContractResolver" />
    /// </summary>
    /// <seealso cref="Nest.JsonNetSerializer.ConnectionSettingsAwareContractResolver" />
    internal sealed class TypeAwareContractResolver : ConnectionSettingsAwareContractResolver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TypeAwareContractResolver" /> class.
        /// </summary>
        /// <param name="connectionSettings">The connection settings.</param>
        public TypeAwareContractResolver(IConnectionSettingsValues connectionSettings)
            : base(connectionSettings)
        {
        }

        /// <summary>
        /// Creates the <see cref="JsonContract" />.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>JsonContract.</returns>
        protected override JsonContract CreateContract(Type objectType)
        {
            var contract = base.CreateContract(objectType);
            // ReSharper disable once InvertIf
            if(contract is JsonContainerContract containerContract)
            {
                if(containerContract.ItemTypeNameHandling == null)
                {
                    containerContract.ItemTypeNameHandling = TypeNameHandling.None;
                }
            }

            return contract;
        }
    }
}
