﻿// ***********************************************************************
// Assembly         : OMCS.Shared.
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-30-2020
// ***********************************************************************
// <copyright file="ElasticRequestBase.cs" company="OMCS.Shared.">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.Collections.Generic;

namespace OMCS.Shared.Elastic.DTO
{
    /// <summary>
    /// The base class for all requests that interact with ElasticSearch.
    /// </summary>
    public class ElasticRequestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ElasticRequestBase" /> class.
        /// </summary>
        /// <param name="authenticationUserName">Name of the authentication user.</param>
        /// <param name="authenticationPassword">Name of the authentication user.</param>
        /// <param name="dataEndpoints">The authentication password.</param>
        protected ElasticRequestBase(string authenticationUserName,
                                     string authenticationPassword,
                                     IList<Uri> dataEndpoints)
        {
            this.AuthenticationUserPassword = authenticationPassword;
            this.AuthenticationUserName = authenticationUserName;
            this.DataEndpoints = dataEndpoints;
        }

        /// <summary>
        /// Gets the name of the elastic search user.
        /// </summary>
        /// <value>The name of the elastic search user.</value>

        public string AuthenticationUserName { get; }

        /// <summary>
        /// Gets or sets the ela search user password.
        /// </summary>
        /// <value>The ela search user password.</value>

        public string AuthenticationUserPassword { get; }

        /// <summary>
        /// Gets the data endpoints.
        /// </summary>
        /// <value>The data endpoints.</value>

        public IList<Uri> DataEndpoints { get; }
    }
}
