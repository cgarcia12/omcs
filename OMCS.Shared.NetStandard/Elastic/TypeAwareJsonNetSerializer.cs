﻿// ***********************************************************************
// Assembly         : OMCS.Shared.
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-27-2020
// ***********************************************************************
// <copyright file="TypeAwareJsonNetSerializer.cs" company="OMCS.Shared.">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using Elasticsearch.Net;
using Nest;
using Nest.JsonNetSerializer;
using Newtonsoft.Json;

namespace OMCS.Shared.Elastic
{
    /// <summary>
    /// Class TypeAwareJsonNetSerializer.
    /// Implements the <see cref="Nest.JsonNetSerializer.ConnectionSettingsAwareSerializerBase" />
    /// </summary>
    /// <seealso cref="Nest.JsonNetSerializer.ConnectionSettingsAwareSerializerBase" />
    internal sealed class TypeAwareJsonNetSerializer : ConnectionSettingsAwareSerializerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TypeAwareJsonNetSerializer" /> class.
        /// </summary>
        /// <param name="builtinSerializer">The builtin serializer.</param>
        /// <param name="connectionSettings">The connection settings.</param>
        public TypeAwareJsonNetSerializer(IElasticsearchSerializer builtinSerializer,
                                          IConnectionSettingsValues connectionSettings)
            : base(builtinSerializer, connectionSettings)
        {
        }

        /// <summary>
        /// Creates the json serializer settings.
        /// </summary>
        /// <returns>JsonSerializerSettings.</returns>
        protected override JsonSerializerSettings CreateJsonSerializerSettings()
        {
            return new JsonSerializerSettings
                   {
                       TypeNameHandling = TypeNameHandling.All,
                       NullValueHandling = NullValueHandling.Ignore,
                       TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
                   };
        }

        /// <summary>
        /// Creates the contract resolver.
        /// </summary>
        /// <returns>ConnectionSettingsAwareContractResolver.</returns>
        protected override ConnectionSettingsAwareContractResolver CreateContractResolver()
        {
            return new TypeAwareContractResolver(this.ConnectionSettings);
        }
    }
}
