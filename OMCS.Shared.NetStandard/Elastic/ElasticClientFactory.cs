﻿// ***********************************************************************
// Assembly         : OMCS.Shared.
// Author           : cgarcia
// Created          : 03-27-2020
//
// Last Modified By : cgarcia
// Last Modified On : 03-27-2020
// ***********************************************************************
// <copyright file="ElasticClientFactory.cs" company="OMCS.Shared.">
//     Copyright (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using Elasticsearch.Net;
using Nest;

namespace OMCS.Shared.Elastic
{
    /// <summary>
    /// Class ElasticBootstrapper.
    /// </summary>
    public static class ElasticClientFactory
    {
        /// <summary>
        /// Creates an <see cref="IElasticClient" /> that persists the '$type' value within an indexed document with a default-index or alias set.
        /// </summary>
        /// <param name="nodeAddresses">The node addresses.</param>
        /// <param name="authenticationUserName">Name of the authentication user.</param>
        /// <param name="authenticationUserPassword">The authentication user password.</param>
        /// <param name="defaultIndexOrAliasName">Default name of the index or alias.</param>
        /// <returns>IElasticClient.</returns>
        public static IElasticClient Create(IEnumerable<Uri> nodeAddresses,
                                            string authenticationUserName,
                                            string authenticationUserPassword,
                                            string defaultIndexOrAliasName)
        {
            IConnectionPool pool = new StaticConnectionPool(nodeAddresses, randomize: false);
            ConnectionSettings cnx = new ConnectionSettings(pool,
                                                            (builtin, settings)=>
                                                                new TypeAwareJsonNetSerializer(builtin, settings));
            cnx.BasicAuthentication(authenticationUserName, authenticationUserPassword);
            if(!string.IsNullOrEmpty(defaultIndexOrAliasName))
            {
                cnx.DefaultIndex(defaultIndexOrAliasName);
            }

            cnx.PrettyJson(true);
            return new ElasticClient(cnx);
        }
    }
}
